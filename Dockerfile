FROM node:lts-alpine@sha256:b2da3316acdc2bec442190a1fe10dc094e7ba4121d029cb32075ff59bb27390a
RUN apk add dumb-init
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY --chown=node:node . .
## Eliminamos la cache y además eliminamos stage con &&
RUN npm cache clean --force
EXPOSE 3000
## No usamos el usuario root 
USER node
## Usamo dumb-init para prevenir problemas con procesos zombies o tiempos de baja de los procesos
CMD ["dumb-init", "node", "index.js"]
